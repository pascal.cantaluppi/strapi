# Strapi website project

Next.js Page with Strapi Headless CMS Backend

<p>
  <img src="https://gitlab.com/pascal.cantaluppi/strapi/-/raw/main/public/strapi.png" alt="Strapi" />
</p>

## Used frameworks

### Next.js

Next.js gives you the best developer experience with all the features you need for production: hybrid static & server rendering, TypeScript support, smart bundling, route pre-fetching, and more. No config needed.

### Strapi

To make it easy for you to get started with GitLab, here's a list of recommended next steps.
Strapi is the leading open-source headless CMS. It’s 100% JavaScript, fully customizable and developer-first.

## Run the app

Run the development server:

```bash
npm run dev
```
